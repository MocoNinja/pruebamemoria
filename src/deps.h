#pragma once
// Standard C / CPP
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
// Custom H
#include "load_parameters.h"
#include "memory.h"
// OS specific
#ifdef _WIN32
#include <windows.h>
#elif defined(__linux__)
#include <sys/sysinfo.h>
#endif
