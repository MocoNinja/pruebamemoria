typedef struct Configuration {
	char* path;
} Configuration;


Configuration* load_parameters(int argc, char** argv);

void clear_configuration(Configuration* config);