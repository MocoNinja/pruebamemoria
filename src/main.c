#include "deps.h"

int main(int argc, char** argv) {
	const Configuration* params = load_parameters(argc, argv);
	const uint64_t memory_total_mb = get_total_system_memory_bytes() / 1024 / 1024;
	const uint64_t  memory_available_mb = get_available_system_memory_bytes() / 1024 / 1024;
	const double free = (double) 100 * memory_available_mb / memory_total_mb;
	printf("TOTAL RAM: %llu [MB(s)] | AVILABLE RAM: %llu [MB(s)] | FREE: %.2f %%\n", memory_total_mb, memory_available_mb, free);
	printf("Read file: %s\n", params->path);
	clear_configuration(params);
	return 0;
}

