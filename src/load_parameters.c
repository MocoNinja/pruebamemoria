#include "deps.h"

Configuration* load_parameters(int argc, char** argv)
{
	// We should validate params
	Configuration* instance = malloc(sizeof(Configuration));
	if (instance == NULL) {
		return NULL;
	}
	// Right now: assume that argv[0] is the string
	const int size = strlen(argv[0]);
	const int target_size = size + 1;
	instance->path = malloc(target_size);
	if (instance->path == NULL) {
		free(instance);
		return NULL;
	}
	// No corre en linux
	//strcpy_s(instance->path, target_size, argv[0]);
	strcpy(instance->path, argv[0]);
	return instance;
}


void clear_configuration(Configuration* config) {
	if (config != NULL) {
		free(config->path);
		free(config);
	}
}