#include "deps.h"

#ifdef _WIN32
unsigned long long get_total_system_memory_bytes() {
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    // bytes
    return statex.ullTotalPhys;
}

unsigned long long get_available_system_memory_bytes() {
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    // bytes
    return statex.ullAvailPhys;
}
#elif defined(__linux__)
unsigned long long get_total_system_memory_bytes() {
    struct sysinfo si;
    sysinfo(&si);
    return si.totalram;
}
unsigned long long get_available_system_memory_bytes() {
    struct sysinfo si;
    sysinfo(&si);
    return si.freeram;
}
#else
#error "Unsupported platform"
#endif
