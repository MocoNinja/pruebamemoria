@echo off
rmdir /s /q build
rmdir /s /q out
mkdir build
mkdir out
cd build
cmake ..
cmake --build .
copy Debug\read_memory.exe ..\out