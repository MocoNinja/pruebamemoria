cmake_minimum_required(VERSION 3.12)
project(read_memory)

file(GLOB SOURCE_FILES src/*.c src/*.h)

add_executable(read_memory ${SOURCE_FILES})