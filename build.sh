#! /bin/sh

rm -rf build
rm -rf out
mkdir build
mkdir out

cd build
cmake ..
make

cp read_memory ../out
